#!/bin/bash

docker rm -f mongo
docker rm -f EDITAL_42020

sleep 5
docker run -d -p 27017:27017 --name mongo --restart=always -v `pwd`/mongo:/data/db mongo
sleep 5
docker run -d -p 1338:1337 --restart=always -e APP_ID=prova -e MASTER_KEY=prova  --link mongo -v `pwd`/parseProva:/parse/cloud --name EDITAL_42020 parseplatform/parse-server

