# EDITAL_4_2020

Processo Seletivo com vista à formação de cadastro de reserva destinado à composição de seu
quadro de colaboradores na Área de Desenvolvimento de Tecnologia - Desenvolvimento de
Aplicativos, para atuar nos projetos do CAEd.


# Orientações
Ficamos felizes pelo seu interesse na vaga. Estamos a proucura de um profissional com habilidade backend e mobile. Abaixo disponibilizamos uma avaliação para que seja possível demonstrar-nos sua desenvoltura como desenvolvedor. Caso não consiga fazer alguma das atividades abaixo, não há problemas. Iremos te avaliar com as atividades que conseguiu desenvolver.


A prova será dividida em duas etapas backend e mobile. A primeira etapa conterá um CRUD em node.js ou parse.js com integração ao BD mongoDB. Por sua vez, a segunda etapa será uma tela com listagem e outra com detalhamento de produto. **Será melhor avaliado caso seja aplicado teste unitário...**

Para axiliar, disponibilizamos um arquivo *dockerParse.sh* com mongo e parseServer configurado.

## Primeira etapa
1. Construa uma API para *Salvar* dados na collection *Instrumentacao* da base *EDITAL_42020*

2. Construa uma API para *Alterar* os dados na collection *Instrumentacao* da base *EDITAL_42020*

3. Construa uma API para *Deletar* os dados na collection *Instrumentacao* da base *EDITAL_42020*

4. Faça uma API que retorne um agrupado de *"NU_TIPO" : "90"*  da Seguinte forma: 
* Descriçoes = Agrupado de *DC_ITEM*
* Ordens = Agrupado de *NU_ORDEM*
* Soma = Soma de registros
~~~
{
  "InstrumentacaoTipo90" :[
    {
      "Descricoes":[
        "ITEM 1: ACENO, TARDE, PADARIA",
        "FLUENCIA EM LEITURA",
        "ITEM 3: PENAS"
       ]
      "Ordens":[5,9,13]
      "Soma": 3
    }
  ]

}
~~~

5. Dado o *NM_ITEM*, faça uma API que retorne a relação entre os *"NU_TIPO" : "90"* com os *"NU_TIPO" : "92"*. Essa relação é feita pelo atributo *NM_ITEM_RELACIONAMENTO* nos registro de *NU_TIPO = 92*
~~~
{
  "Descricoes":[
    "ITEM 1: ACENO, TARDE, PADARIA",
    "FLUENCIA EM LEITURA",
    "ITEM 3: PENAS"
    ]
  "Ordens":[5,9,13]
  "Soma": 3
}
~~~

## Segunda etapa
Faremos MVP de um aplicativo para divulgação de carros. Esse aplicativo terá duas telas. Uma para listagem de todos os carros e outra para os detalhes do carro. 

`Designer`: https://xd.adobe.com/view/969b35a8-2a26-4a10-604b-d0e7e53eaeef-e0aa/

1. Para tela de listagem do carro, temos a seguinte API REST:
* **URL:** http://www.mocky.io/v2/5e6be6e82d00004a008e9c91
* **Method**: `GET`
~~~JSON
{
  "result": {
    "carros": [
      {
        "id": 0,
        "nome": "Hyundai IX35",
        "subTitulo": "2.0 MPFI GL 16V FLEX 4P AUTOMÁTICO",
        "valor": 78000.6,
        "ano": "2016/2017",
        "cidade":"Juiz de Fora",
        "urlFoto": "https://image.webmotors.com.br/_fotos/anunciousados/gigante/2020/202002/20200204/hyundai-ix35-2.0-mpfi-gl-16v-flex-4p-automatico-wmimagem09201569230.jpg?s=fill&w=552&h=414&q=60"
      }
    ]
  }
}
~~~


2. Para tela de detalhamento do produto, usaremo a seguinte: 

* **URL:** http://www.mocky.io/v2/5e6be6f82d000032008e9c92
* **Method**: `GET`
~~~JSON
{
  "result": {
    "carro":{
        "id": 0,
        "nome": "Hyundai IX35",
        "subTitulo": "2.0 MPFI GL 16V FLEX 4P AUTOMÁTICO",
        "valor": 78000.6,
        "ano": "2016/2017",
        "cidade":"Juiz de Fora",
        "urlFoto": "https://image.webmotors.com.br/_fotos/anunciousados/gigante/2020/202002/20200204/hyundai-ix35-2.0-mpfi-gl-16v-flex-4p-automatico-wmimagem09201569230.jpg?s=fill&w=552&h=414&q=60",
        "km": 49000,
        "finalPlaca":8,
        "unicoDono":true,
        "cor":"Preto",
        "aceitaTroca":true,
        "cambio":"Automático",
        "carroceria": "Utilitário esportivo",
        "usuario":{
            "nome":"Pravida Motors",
            "cidade":"Juiz de Fora",
            "telefone":"(32) 98567-2154"
        }
      }
  }
}
~~~

# Entrega da avaliação:
* Faça um `Merge Request` para esse projeto. Caso haja dificuldade, inclua permissão no seu repositório para romulo.barbosa@caed.ufjf.br e 
* Envie um email para processoseletivo@fundacaocaed.org.br, até as 18:00 horas do dia 16/04/2020, com assunto **EDITAL_04_2020** informando o término da avaliação.

# Dicas de lib e material para prova:
#### Primeira etapa
* Node.js - https://nodejs.org/en/docs/
* Jests - https://jestjs.io/docs/en/getting-started
* MongoDb - https://docs.mongodb.com/
* Rest - https://www.w3.org/2001/sw/wiki/REST

#### Segunda etapa
* Kotlin - https://developer.android.com/kotlin?hl=pt-br
* Glide - https://bumptech.github.io/glide/
* Jetpack - https://developer.android.com/jetpack?hl=pt-br

#### Entrega da avaliação
* Merge Request - https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html

**Dica de programa para auxiliar o desenvolvimento**
* NosqlBooster - https://nosqlbooster.com/downloads
* Android Studio - https://developer.android.com/studio?hl=pt-br
* VsCode - https://code.visualstudio.com/download